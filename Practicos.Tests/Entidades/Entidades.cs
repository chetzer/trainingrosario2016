﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Auto
    {
        private double Velocidad = 0;
        public string Marca { get; set; }
        public string Color { get; set; }
        public string Modelo { get; set; }
        private string Patente { get; set; }
        public Auto() { }
        public Auto(double velocidad,string marca,string color,string modelo, string patente)
        {

            this.Velocidad = velocidad;
            this.Marca = marca;
            this.Color = color;
            this.Patente = patente;
            this.Modelo = modelo;
        }

        public double velocidad
        {
            get
            {
                return Velocidad;
            }
        }
       
        private double Acelerar(double acelerado)
        {
           
            return this.Velocidad = this.Velocidad + acelerado;
        }

        private double Frenar(double frenado)
        {
            
            return this.Velocidad = this.Velocidad + frenado;
        }
    }

    public class Producto
    {
        public DateTime FechaCaducidad { get; set; }
        public double NroLote { get; set; }
        public Producto() { }
        public Producto (DateTime fcaducidad, double nrolote)
        {
            this.FechaCaducidad = fcaducidad;
            this.NroLote = nrolote;
        }
        public virtual void MostrarDatos()
        {
            Console.WriteLine("Fecha de caducidad {0}", FechaCaducidad);
            Console.WriteLine("Número de lote {0}", NroLote);
        }
    }
    public class ProductoFresco : Producto
    {
        public DateTime FechaEnvasado { get; set; }
        public string PaisOrigen { get; set; }
        public ProductoFresco() { }
        public ProductoFresco(DateTime fcaducidad, double nrolote, DateTime fenvasado, string paisorigen):base (fcaducidad, nrolote)
        {           
            this.FechaEnvasado = fenvasado;
            this.PaisOrigen = paisorigen;
        }
        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Fecha de envasado {0}", FechaEnvasado);
            Console.WriteLine("País de Origen {0}", PaisOrigen);
        }
    }
    public class ProductoRefrigerado : Producto
    {
        public string CodigoOrganismo { get; set; }
        public ProductoRefrigerado() { }
        public ProductoRefrigerado(DateTime fcaducidad, double nrolote, string cOrganismo):base(fcaducidad, nrolote)
        {
            this.NroLote = nrolote;
            this.CodigoOrganismo = cOrganismo;
        }
        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Código de Organismo {0}", CodigoOrganismo);
        }
    }
       
   public class ProductoCongelado : Producto
        {
            public string Temperatura { get; set; }
            public ProductoCongelado() { }
            public ProductoCongelado(DateTime fcaducidad, double nrolote,string temperatura) : base(fcaducidad, nrolote)
        {
                this.Temperatura = temperatura;
            }
        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Temperatura de mantenimiento recomendada {0}", Temperatura);
        }
    }
    }
