﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Practicos.Tests
{
    [TestClass]
    public class Practico1
        
    {
        [TestMethod]
        public void MenordeTresNumeros()
        {
            var listNumeros = new List<int>();
            var listaVacia = new List<int>();
            listNumeros.Add(1);
            listNumeros.Add(2);
            listNumeros.Add(3);
            listNumeros.Add(-5);
            Assert.AreEqual(0,Practicos.Calculos.CalcularMenordeNNumeros(listaVacia));
            Assert.AreEqual(-5, Practicos.Calculos.CalcularMenordeNNumeros(listNumeros));

        }

        [TestMethod]
        public void MayordeTresNumeros()
        {
            var listNumeros = new List<int>();
            var listaVacia = new List<int>();
            listNumeros.Add(1);
            listNumeros.Add(2);
            listNumeros.Add(3);
            listNumeros.Add(-5);
            Assert.AreEqual(0, Practicos.Calculos.CalcularMayordeNNumeros(listaVacia));
            Assert.AreEqual(3, Practicos.Calculos.CalcularMayordeNNumeros(listNumeros));
        }

        [TestMethod]
        public void PromedioDeTresNumeros()
        {

            var listNumeros = new List<int>();
            var listaVacia = new List<int>();
            listNumeros.Add(1);
            listNumeros.Add(2);
            listNumeros.Add(3);
            Assert.AreEqual(0, Practicos.Calculos.CalcularMayordeNNumeros(listaVacia));
            Assert.AreEqual(2, Practicos.Calculos.PromedioDeNNumeros(listNumeros));
        }

        [TestMethod]
        public void NumeroPar()
        {
            Assert.IsTrue(Practicos.Calculos.EsNumeroPar(250));
            Assert.IsFalse(Practicos.Calculos.EsNumeroPar(177));
            Assert.IsFalse(Practicos.Calculos.EsNumeroPar(-7));
            Assert.IsTrue(Practicos.Calculos.EsNumeroPar(0));
        }
        [TestMethod]
        public void EsVocal()
        {

            Assert.IsTrue(Practicos.Calculos.EsVocal("a"));
            Assert.IsTrue(Practicos.Calculos.EsVocal("e"));
            Assert.IsTrue(Practicos.Calculos.EsVocal("i"));
            Assert.IsTrue(Practicos.Calculos.EsVocal("o"));
            Assert.IsTrue(Practicos.Calculos.EsVocal("u"));
            Assert.IsFalse(Practicos.Calculos.EsVocal("b"));
            Assert.IsFalse(Practicos.Calculos.EsVocal("%"));
            Assert.IsFalse(Practicos.Calculos.EsVocal(" "));
            Assert.IsTrue(Practicos.Calculos.EsVocal("A"));
            Assert.IsTrue(Practicos.Calculos.EsVocal("E"));
            Assert.IsTrue(Practicos.Calculos.EsVocal("I"));
            Assert.IsTrue(Practicos.Calculos.EsVocal("O"));
            Assert.IsTrue(Practicos.Calculos.EsVocal("U"));
        }
        [TestMethod]
        public void CalcularProducto()
        {
            Assert.AreEqual(6, Practicos.Calculos.ProductoPorSuma(2, 3));
            Assert.AreEqual(0, Practicos.Calculos.ProductoPorSuma(2, 0));
            Assert.AreEqual(-6, Practicos.Calculos.ProductoPorSuma(-3, 2));
            Assert.AreEqual(9, Practicos.Calculos.ProductoPorSuma(-3, -3));
            Assert.AreEqual(-15, Practicos.Calculos.ProductoPorSuma(5, -3));
        }

        [TestMethod]
        public void CalcularDivision()
        {
            Assert.AreEqual(5, Practicos.Calculos.DivisionPorResta(10, 2));
            Assert.AreEqual(0, Practicos.Calculos.DivisionPorResta(0, 10));
            Assert.AreEqual(-5, Practicos.Calculos.DivisionPorResta(-15, 3));
            Assert.AreEqual(5, Practicos.Calculos.DivisionPorResta(-25, -5));
            Assert.AreEqual(-5, Practicos.Calculos.DivisionPorResta(15, -3));
        }
        [TestMethod]
        public void FinSerie()
        {
            Assert.AreEqual(6, Practicos.Calculos.FinSerie(21));
            Assert.AreEqual(0, Practicos.Calculos.FinSerie(0));
            Assert.AreEqual(2, Practicos.Calculos.FinSerie(3));
            Assert.AreEqual(7, Practicos.Calculos.FinSerie(23));
        }
        [TestMethod]
        public void ValidarCantidades()
        {
            var listNumeros = new List<int>();
            listNumeros.Add(1);
            listNumeros.Add(2);
            listNumeros.Add(3);
            listNumeros.Add(-3);
            listNumeros.Add(-7);
            listNumeros.Add(-1);
            listNumeros.Add(-15);
            listNumeros.Add(0);
            Assert.AreEqual(3, Practicos.Calculos.CantidadNumerosPositivos(listNumeros));
            Assert.AreEqual(4, Practicos.Calculos.CantidadNumerosNegativos(listNumeros));
            Assert.AreEqual(2, Practicos.Calculos.CantidadNumerosPares(listNumeros));
            Assert.AreEqual(6, Practicos.Calculos.CantidadNumerosImpares(listNumeros));
        }
        [TestMethod]
        public void OperacionesAritmeticas()
        {
            Assert.AreEqual(6, Practicos.Calculos.SumaDosNumeros(2, 4));
            Assert.AreEqual(-1, Practicos.Calculos.SumaDosNumeros(0, -1));
            Assert.AreEqual(-7, Practicos.Calculos.SumaDosNumeros(-2, -5));
            Assert.AreEqual(2, Practicos.Calculos.RestaDosNumeros(5, 3));
            Assert.AreEqual(-1, Practicos.Calculos.RestaDosNumeros(2, 3));
            Assert.AreEqual(0, Practicos.Calculos.RestaDosNumeros(5, 5));
            Assert.AreEqual(0, Practicos.Calculos.ProductoDosNumeros(1, 0));
            Assert.AreEqual(-5, Practicos.Calculos.ProductoDosNumeros(1, -5));
            Assert.AreEqual(10, Practicos.Calculos.ProductoDosNumeros(2, 5));
            Assert.AreEqual(0, Practicos.Calculos.DivisionDosNumeros(5, 0));
            Assert.AreEqual(2, Practicos.Calculos.DivisionDosNumeros(10, 5));
            Assert.AreEqual(-3, Practicos.Calculos.DivisionDosNumeros(12, -4));
        }
        [TestMethod]
        public void SumaParesUntil25()
        {
            Assert.AreEqual(156, Practicos.Calculos.SumarParesUntil25());
        }
    }
}
