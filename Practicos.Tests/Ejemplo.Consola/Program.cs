﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Ejemplo.Consola
{

    public class Program

    {
        static void Main(string[] args)
        {
            autos();
            productos();
        }

        static void autos()
        {
            Auto auto = new Auto();
            Console.WriteLine("Ingresar Marca");
            string marca = Console.ReadLine();
            Console.WriteLine("Ingresar Color");
            string color = Console.ReadLine();
            auto.Marca = marca;
            auto.Color = color;
            Console.ReadKey();//para que no salga de inmediato
        }

        static void productos()
        {

            var productos = new List<Producto>();
            productos.Add(new ProductoCongelado());
            productos.Add(new ProductoCongelado());
            productos.Add(new ProductoFresco());
            productos.Add(new ProductoFresco());

            var produtosCongelados =
                productos.OfType<ProductoCongelado>();
            var produtosFrescos =
                productos.OfType<ProductoFresco>()
                .Where(p => p.PaisOrigen == "Argentina")
                .ToList();


        }
        static void ingresarfresco()
        {
            Console.WriteLine("Ingresar producto");
            //crear la lista e ir llamando a los new en el foreach
        }
    }
      