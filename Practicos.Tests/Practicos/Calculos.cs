﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practicos
{
    public class Calculos
    {
        private static int ultimo;
        public static int CantidadNumerosNegativomeros;

        public static int CalcularMenordeNNumeros(List<int> numeros)
        {
            if (numeros.Count() == 0)
                 { return 0; }
            else
            {
                var menor = numeros[0];
                foreach (int num in numeros)
                {
                    if (menor > num)
                        menor = num;
                }
                return menor;
            }
        }

        public static int CalcularMayordeNNumeros(List<int> numeros)
        {
            if (numeros.Count() == 0)
            { return 0; }
            else
            {
                var menor = numeros[0];
                foreach (int num in numeros)
                {
                    if (menor < num)
                        menor = num;
                }
                return menor;
            }
        }

        public static int PromedioDeNNumeros(List<int> numeros)
        {
            if (numeros.Count() == 0)
            {
                return 0;
            }
            else
            {
                var cantidadDeRegistros = numeros.Count();
                var suma = 0;
                foreach (int num in numeros)
                {
                    suma = suma + num;
                }
                return suma / cantidadDeRegistros;
            }
        }

        public static bool EsNumeroPar(int numpar)
        {

            if (numpar % 2 == 0)
                return true;
            else
                return false;
        }

        public static bool EsVocal(string letra)
        {
            if (letra == "a" || letra == "e" || letra == "i" || letra == "o" || letra == "u" || letra == "A" || letra == "E" || letra == "I" || letra == "O" || letra == "U")
                return true;
            else
                return false;
        }

        public static int ProductoPorSuma(int num1, int num2)
        {
            var producto = 0;
            var contador =num2;
            if (contador < 0) contador = -num2;
         
            for (int i=1; i <= contador; i++)
            {
                 producto = producto + num1;
            }
            if (num1 < 0 & num2 < 0) producto=-producto;
            if (num1 > 0 & num2 < 0) producto=-producto;
            return producto;
        }

        public static int FinSerie(int num)
        {
            int x = 0;
            int suma = 0;
            do
            {
                suma = suma + x;
                int ultimo = x;
                x++;
            } while (suma < num);
            return x-1;
        }

        public static int DivisionPorResta(int num1, int num2)
        {
            var resultado = num1;
            var contador = num2;
            if (contador < 0) contador = -num2;
            var cociente = num1 / contador;
            for (int i = 1; i < contador; i++)
            {
                resultado = resultado - cociente;
            }
            if (num1 < 0 & num2 < 0) resultado = -resultado;
            if (num1 > 0 & num2 < 0) resultado = -resultado;
            return resultado;
        }

        public static int CantidadNumerosPositivos(List<int> numeros)
        {
            var cantidadPositivos = 0;
            foreach(int num in numeros)
            {
                if (num > 0) cantidadPositivos = cantidadPositivos+1;
            }
            return cantidadPositivos;
        }

        public static int CantidadNumerosNegativos(List<int> numeros)
        {
            var cantidadNegativos = 0;
            foreach (int num in numeros)
            {
                if (num < 0) cantidadNegativos = cantidadNegativos + 1;
            }
            return cantidadNegativos;
        }

        public static int CantidadNumerosPares(List<int> numeros)
        {
            var cantidadPares = 0;
            foreach (int num in numeros)
            {
                if (Calculos.EsNumeroPar(num)==true) cantidadPares = cantidadPares + 1;
            }
            return cantidadPares;
        }

        public static int CantidadNumerosImpares(List<int> numeros)
        {
            var cantidadImpares = 0;
            foreach (int num in numeros)
            {
                if (Calculos.EsNumeroPar(num) == false) cantidadImpares = cantidadImpares + 1;
            }
            return cantidadImpares;
        }

        public static int RestaDosNumeros(int n1, int n2)
        {
            return n1 - n2;
        }

        public static int SumaDosNumeros(int n1, int n2)
        {
            return n1 + n2;
        }

        public static int ProductoDosNumeros(int n1, int n2)
        {
            return n1 * n2;
        }

        public static int DivisionDosNumeros(int n1, int n2)
        {
            if (n2 != 0)
                return n1 / n2;
            else
                return 0;
        }

        public static int SumarParesUntil25()
        {
            int suma = 0;
            for (int i = 1; i < 25; i++)
            {
                if (Calculos.EsNumeroPar(i)==true) suma=suma+i;
            }
            return suma;
        }
    }
}
